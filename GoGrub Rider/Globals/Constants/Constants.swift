//
//  Constants.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation



//mark:- API'S Url
let LOGIN = "http://207.148.113.150/index.php/api/v1/rider/login"
let SIGNUP = "http://207.148.113.150/api/v1/rider/register"
let RESET_PASSWORD = ""
let NOTIFY_ORDER = "http://207.148.113.150/index.php/api/v1/rider/current/notify/order?"
let UPDATE_PROFILE = ""
let HISTORY = "http://207.148.113.150/api/v1/rider/orders/history"
let CURRENT_ORDER = "http://207.148.113.150/api/v1/rider/current/order"





var rider_id = ""
var rider_Name = ""
var rider_Email = ""
var rider_mobile = ""
var rider_rating =  ""
var applied_as_chef = false
var avg_reply_time = 0
var success_percentage =  ""

var documentVerification = [#imageLiteral(resourceName: "ic_gallery"),
                     #imageLiteral(resourceName: "ic_gallery"),
                     #imageLiteral(resourceName: "ic_gallery"),
                     #imageLiteral(resourceName: "ic_gallery")]

var currentRiderLat = CLLocationDegrees()
var currentRiderLong = CLLocationDegrees()

var LeftItemImage = [#imageLiteral(resourceName: "home (2)"),
                     #imageLiteral(resourceName: "profile"),
                     #imageLiteral(resourceName: "history"),
                     #imageLiteral(resourceName: "logout")]

var leftMenuItemTitles = ["Home","Profile","History","Logout"]


var fcm_Token = ""
var device_id = ""
