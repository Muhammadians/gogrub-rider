//
//  CustomizationTextfield.swift
//  GoGrub Rider
//
//  Created by apple on 10/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
@IBDesignable

class CustomizationTextfield: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        borderStyle = .none
        layer.cornerRadius = bounds.height / 2
        //layer.borderWidth = 1.0
        //        layer.borderColor = UIColor.init(colorLiteralRed: 241/256, green: 241/256, blue: 241/256, alpha: 1).cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 4.0, height: 4.0)
        layer.shadowRadius = 2
        layer.masksToBounds = false
        layer.shadowOpacity = 0.3
        // set backgroundColor in order to cover the shadow inside the bounds
        layer.backgroundColor = UIColor.white.cgColor
    }

}
