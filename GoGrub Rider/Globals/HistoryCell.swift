//
//  HistoryCell.swift
//  GoGrub Rider
//
//  Created by apple on 10/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell
{
    @IBOutlet weak var idNumber: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var chefName: UILabel!
    

}
