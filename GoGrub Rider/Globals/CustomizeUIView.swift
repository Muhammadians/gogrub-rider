//
//  CustomizeUIView.swift
//  GoGrub Rider
//
//  Created by apple on 10/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
@IBDesignable

class CustomizeUIView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
}
