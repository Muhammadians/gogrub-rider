//
//  ModelData.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import SwiftyJSON



struct firebaseLatLng
{
    var Lat : String?
    var Lng : String?
   
    
    
    init(dict : [String:AnyObject])
    {
        self.Lat = dict["Lat"] as? String
        self.Lng = dict["Lng"] as? String
       
    }
    
}








struct product
{
   
    
    var id : String?
    var order_id : String?
    var product_id : String?
    var qty : String?
    var price : String?
    var special_instructions : String?
    var added_by : String?
    var created_at : String?
    var updated_at : String?
    var product : String?
    var total_price : String?
    
  
    
    
    init(){
        
    }
    
    init(json : JSON)
    {
        
        self.id = String(json["id"].intValue)
        self.product_id = String(json["product_id"].intValue)
        self.order_id = String(json["order_id"].intValue)
        self.price = json["price"].stringValue
        self.qty = String(json["qty"].intValue)
        self.product = json["product"].stringValue
         self.total_price = String(json["total_price"].intValue)
        
    }
    
}


struct history
{

    
    var id : Int?
    var rider_id : Int?
    var order_id : Int?
    var is_completed : Int?
    var created_at : String?
    var updated_at : String?
    
    var Orders_id : Int?
    var chef_full_name : String?
    var customer_full_name : String?
    var total : String?
    var posted_at : String?
    var avg_rating : String?
    var has_rated : Bool?
    
    var detail_Id : Int?
    var detail_Order_Id : Int?
    var product_id : Int?
    var qty : Int?
    
    var price : Int?
    var added_by : Int?
    var total_price : Int?
    var product : String?
    var special_instructions : String?
    var detail_created_at : String?
    var detail_updated_at : String?
    
    
    init(){
        
    }
    
    init(json : JSON)
    {
        
        self.id = json["id"].int
        self.rider_id = json["rider_id"].intValue
        self.order_id = json["order_id"].intValue
        self.is_completed = json["is_completed"].intValue
        self.created_at = json["created_at"].stringValue
        self.updated_at = json["updated_at"].stringValue
    
      let Orders = json["orders"]
        self.Orders_id = Orders["id"].intValue
        self.chef_full_name = Orders["chef_full_name"].stringValue
        self.customer_full_name = Orders["customer_full_name"].stringValue
        self.total = Orders["total"].stringValue
        self.posted_at = Orders["posted_at"].stringValue
        self.avg_rating = Orders["avg_rating"].stringValue
        self.has_rated = Orders["has_rated"].boolValue
    
        
     let details = Orders["detail"]
        let detail = details[0]
        
        self.detail_Id = detail["id"].int
        self.product_id = detail["product_id"].intValue
        self.detail_Order_Id = detail["order_id"].intValue
        self.qty = detail["qty"].intValue
        self.price = detail["price"].int
        self.added_by = detail["added_by"].intValue
        self.total_price = detail["total_price"].intValue
        self.is_completed = detail["is_completed"].intValue
        let products = detail["product"]
            self.product = products["name"].stringValue
    
    }
    
}


struct notifyOrder
{
 
  
    
    var id : String?
    var customer_id : String?
    var chef_id : String?
    var orderstatus_id : String?
    var invoice_num : String?
    var special_instructions : String?
    
    var gogrub_commission : String?
    var chef_full_name : String?
    var chef_phone : String?
    var chef_email : String?
    var chef_location_Lat : Double?
    var chef_location_Long : Double?
    
    var customer_full_name : String?
    var customer_phone : String?
    var customer_email : String?
    var customer_address : String?
    var customer_city : String?
    var customer_province : String?
    var customer_country : String?
    var customer_location_Lat : Double?
    var customer_location_Long : Double?
    
    
    var estimate_delivery_mins : String?
    var coupon_code : String?
    var discount : String?
    var discount_type : String?
    var delivery_charges : String?
    var subtotal : String?
    var payment_method : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var total : String?
    var posted_at : String?
    var avg_rating : String?
    var has_rated : Bool?
    
    
    var detail_Id : String?
    var detail_Order_Id : String?
    var product_id : String?
    var qty : String?
    
    var price : String?
    var added_by : String?
    var total_price : String?
    var product : String?
    var detail_special_instructions : String?
    var detail_created_at : String?
    var detail_updated_at : String?
    
   
    
    
    
    init(){
        
    }
    
    init(json : JSON)
    {
        
        self.id = String(json["id"].intValue)
        self.customer_id = String(json["customer_id"].intValue)
        self.chef_id = String(json["chef_id"].intValue)
        self.orderstatus_id = String(json["orderstatus_id"].intValue)
        self.invoice_num = json["invoice_num"].stringValue
        self.special_instructions = json["special_instructions"].stringValue
        self.gogrub_commission = String(json["gogrub_commission"].intValue)
        self.chef_full_name = json["chef_full_name"].stringValue
        self.chef_phone = json["chef_phone"].stringValue
        self.chef_email = json["chef_email"].stringValue
        
        let chef_location = json["chef_location"]
        let coordinates = chef_location["coordinates"]
        
        chef_location_Lat = coordinates[0].doubleValue
        chef_location_Long = coordinates[1].doubleValue
        

        self.customer_full_name = json["customer_full_name"].stringValue
        self.customer_phone = json["customer_phone"].stringValue
        self.customer_address = json["customer_address"].stringValue
        self.customer_email = json["customer_email"].stringValue
        self.customer_city = json["customer_city"].stringValue
        self.customer_province = json["customer_province"].stringValue
        self.customer_country = json["customer_country"].stringValue
        
        let customer_location = json["customer_location"]
        let C_coordinates = customer_location["coordinates"]
        
        customer_location_Lat = C_coordinates[0].doubleValue
        customer_location_Long = C_coordinates[1].doubleValue
        
        
      
        
        self.estimate_delivery_mins = String(json["estimate_delivery_mins"].intValue)
        self.coupon_code = String(json["coupon_code"].intValue)
        self.discount = String(json["discount"].intValue)
        self.discount_type = json["discount_type"].stringValue
        self.delivery_charges = String(json["delivery_charges"].intValue)
        self.subtotal = String(json["subtotal"].intValue)
        self.payment_method = json["payment_method"].stringValue
        self.created_at = json["created_at"].stringValue
        self.updated_at = json["updated_at"].stringValue
        self.deleted_at = json["deleted_at"].stringValue
        self.total = String(json["total"].intValue)
        self.posted_at = json["posted_at"].stringValue
        self.avg_rating = json["avg_rating"].stringValue
        self.has_rated = json["has_rated"].boolValue
        
    
        
       
        
        let detail = json["detail"]
        
        self.detail_Id = String(detail["id"].intValue)
        self.product_id = String(detail["product_id"].intValue)
        self.detail_Order_Id = String(detail["order_id"].intValue)
        self.qty = String(detail["qty"].intValue)
        self.price = String(detail["price"].intValue)
        self.added_by = String(detail["added_by"].intValue)
        self.total_price = String(detail["total_price"].intValue)
        self.detail_special_instructions = detail["special_instructions"].stringValue
        self.product = detail["product"].stringValue
        
        
    }
    
}
