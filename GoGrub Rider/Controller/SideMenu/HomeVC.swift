//
//  HomeVC.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 25/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SideMenu
import SideMenuSwift


class HomeVC: UIViewController {
    
    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets

    @IBOutlet weak var menubtn: UIButton!
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
//        dashBoardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
//        earningView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
//        memberView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
//
     //   Localize.setCurrentLanguage(ENGLISH)
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        
       // self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addShop))
        
    }
    
//    @objc func addShop()
//    {
//        print("dsfl")
//    }
    
    override func viewWillAppear(_ animated: Bool)
    {
//        modeChange()
//        USER_DETAIL_API()
        //if checkMode{
          //  self.menubtn.setImage(#imageLiteral(resourceName: "menu"), for: UIControl.State.normal)
      //  }else{
            self.menubtn.setImage(#imageLiteral(resourceName: "home_map_button"), for: UIControl.State.normal)
       // }
//
//        self.labelDashBoard.text = "Dashboard".localized()
//        self.labelEarnings.text = "Earnings".localized()
//        self.activeMembers.text = "Active members".localized()
//
    }
    
//    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
//        let tag = gestureRecognizer.view?.tag
//        switch tag! {
//        case 1 :
//            let move = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
//            self.navigationController?.pushViewController(move, animated: true)
//
//        case 2 :
//
//            loader()
//            CURRENTLY_MONTHLY_EARNING_API()
//
//        case 3 :
//            let memberVC = MemberVC.Member()
//            self.navigationController?.pushViewController(memberVC, animated: true)
//
//        case 4:
//            break
//        default:
//            print("default")
//        }
//    }
//
    
    @IBAction func leftMenu(_ sender: Any)
    {
    }
//    @objc func changeLanguage(){
//        print("LanguageChanged")
//        viewWillAppear(true)
//    }
    
    
    
}




//
//extension HomeVC{
//
//    func USER_DETAIL_API()
//    {
//
//        let url = URL(string: USER_DETAIL + user_Id)
//        URLSession.shared.dataTask(with: url!) { (data, response, err) in
//            guard let data = data else{return}
//            do{
//                let json = try JSON(data:data)
//                DispatchQueue.main.async {
//                    user_gender = json["gender"].stringValue
//                    user_mail = json["email"].stringValue
//                    user_name = json["name"].stringValue
//                    user_cell = json["phone"].stringValue
//                    user_dp = json["image"].stringValue
//                    self.PROFILE_API()
//
//                }}catch{print("error")
//            }}.resume()
//
//    }
//
//    func PROFILE_API()
//    {
//        loader()
//        let url = URL(string: UPDATE_PROFILE + user_Id)
//
//        var request = URLRequest(url:url!)
//        request.httpMethod = "POST"
//        let parameterToSend = "firstname=" + user_name + "&lastname=" + "" + "&email=" + user_mail + "&phone=" + user_cell + "&ref_id=" + user_ref_id + "&gender=" + user_gender + "&notification_id=" + NotiToken
//        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
//        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
//
//            guard let _:Data = data else{return}
//            if error != nil{}
//            let json : Any?
//            do{
//                json = try JSONSerialization.jsonObject(with: data!, options: [])
//            }catch{return}
//            guard let server_Response = json as? NSDictionary else{return}
//            if (server_Response["message"] as? String) != nil{
//                DispatchQueue.main.async {
//                    SVProgressHUD.dismiss()
//
//                }
//
//            }}
//        task.resume()
//
//    }
//
//    func CURRENTLY_MONTHLY_EARNING_API()
//    {
//
//        let url = URL(string: CURRENTLY_MONTHLY_EARNING + user_Id)
//        URLSession.shared.dataTask(with: url!) { (data, response, err) in
//            guard let data = data else{return}
//            do{
//                let json = try JSON(data:data)
//                let res = json[0]
//                month = res["month"].stringValue
//                earnings = res["earnings"].stringValue
//
//                DispatchQueue.main.async {
//                    SVProgressHUD.dismiss()
//                    let move = self.storyboard?.instantiateViewController(withIdentifier: "EarningsVC") as! EarningsVC
//                    self.navigationController?.pushViewController(move, animated: true)
//                }
//
//            }catch{print("error")}}.resume()
//
//    }
//
//
//}
//
