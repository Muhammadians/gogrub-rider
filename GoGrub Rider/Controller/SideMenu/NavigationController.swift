//
//  NavigationController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 25/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//


import UIKit

class NavigationController: UINavigationController{
    open override var childForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
    
    open override var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
}
