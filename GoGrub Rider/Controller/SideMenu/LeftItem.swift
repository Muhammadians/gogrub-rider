//
//  LeftItem.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 25/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class LeftItem: UITableViewCell {
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
