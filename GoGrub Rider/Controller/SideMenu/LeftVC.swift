//
//  LeftVC.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 25/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import SVProgressHUD
import Alamofire

class LeftVC: UIViewController {
    
    @IBOutlet weak var picture: CustomizationImageView!
    @IBOutlet weak var name: UILabel!
    var ref = DatabaseReference.init()
    @IBOutlet weak var leftMenuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        self.name.text = rider_Name
        
        self.leftMenuTableView.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        leftMenuTableView.reloadData()
    }
    class func Left()->LeftVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftVC") as! LeftVC
    }
    
}
extension LeftVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuItemTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftItem") as! LeftItem
        
              //  cell.itemTitle.text = leftMenuItemTitles[indexPath.row].localized()
      
            cell.itemTitle.text = leftMenuItemTitles[indexPath.row]
      
        
//        if indexPath.row < 8{
        cell.itemImage.image = LeftItemImage[indexPath.row]
//        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0:print("")
             let goToTabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "main") as! MainViewController
            present(goToTabbarVC, animated: true, completion: nil)
           // let vc = GoldenTripsVC.GoldenTrips()
            //self.navigationController?.pushViewController(vc, animated: true)
            
          //  performSegue(withIdentifier: "home", sender: self)
            
        case 1:

            performSegue(withIdentifier: "profile", sender: self)
          

        case 2:
            
            performSegue(withIdentifier: "history", sender: self)
           // let vc = NotificationsVC.Notifications()
            //self.navigationController?.pushViewController(vc, animated: true)

        case 3:
            
            logout()
           
            
           // let vc = TripGallerVC.TripGaller()
            //self.navigationController?.pushViewController(vc, animated: true)
//
//        case 4:
//            let vc = AddMemberVC.AddMember()
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        case 5:
//            let vc = ParametersVC.Parameters()
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        case 6:
//            let vc = MyProfileVC.MyProfile()
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        case 7:
//            let goToTabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            present(goToTabbarVC, animated: true, completion: nil)
//
//
        default:
            print("Default")
        }
    }
    
    
    }





extension LeftVC{
    
    func logout()
    {
        
        
        loader()
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
        ]
        
        Alamofire.request("http://207.148.113.150/index.php/api/v1/logout", method: .post, parameters: ["device_id":device_id], headers: headers).responseData{
            response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                
                print(json["message"].stringValue)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
                self.present(vc, animated: true) {
                    self.ref.child("Online_Drivers").child(rider_id).removeValue()
                    currentRiderLong = 0.0
                    currentRiderLat = 0.0
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        
        
        
    }
    
}
