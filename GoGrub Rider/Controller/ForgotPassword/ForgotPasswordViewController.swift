//
//  ForgotPasswordViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 26/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON



class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var txtEmail: CustomizationTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Forgot Password"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow"), style: .plain, target: self, action: #selector(Menu))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cart"), style: .plain, target: self, action: #selector(icon))
        
    }
    
    @objc func Menu(){
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    @objc func icon(){
        
        
        //  self.navigationController?.popViewController(animated: true)
    }

    @IBAction func tappedBtn(_ sender: Any) {
        
        
        
        
        
    }
}


extension ForgotPasswordViewController{
    
    
    func forgotPassword()
    {  loader()
            let url = URL(string: SIGNUP)
            var request = URLRequest(url:url!)
            request.httpMethod = "POST"
            let parameterToSend = "full_name=" + txtEmail.text! + "&password_confirmation=" + txtEmail.text!
        
            request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                guard let _:Data = data else{return}
                if error != nil{
                    // alertPopup(title: error as! String)
                }
                
                let json : Any?
                
                do{
                    json = try JSONSerialization.jsonObject(with: data!, options: [])
                }catch{
                    return
                }
                
                guard let server_Response = json as? NSDictionary else{return}
                if let data_block = server_Response["message"] as? String{
                    
                    DispatchQueue.main.async {
                    
                        
                        SVProgressHUD.dismiss()
                        let alerts = UIAlertController.init(title: "You have registered successfully. Please enter 4 digit pin recieved on your mobile number!" , message: "", preferredStyle: .alert)
                        alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                            self.performSegue(withIdentifier: "home", sender: self)
                        }))
                        self.present(alerts, animated: true, completion: nil)
                        
                    }
                }
                
            }
            
            task.resume()
            
        }

    
    
    
}
