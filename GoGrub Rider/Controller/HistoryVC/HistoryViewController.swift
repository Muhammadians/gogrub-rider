//
//  HistoryViewController.swift
//  GoGrub Rider
//
//  Created by apple on 10/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire

class HistoryViewController: UIViewController{

    //Mark :- Variables
    
    //Mark :- Arrays
    var historyArr = [history]()
    //Mark :- outlets
    @IBOutlet weak var mainVu: UIView!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "History"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cart"), style: .plain, target: self, action: #selector(icon))
       getDataOnServers()
        
    }
    
    @objc func back(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func icon(){}
    
}


extension HistoryViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryCell
        
        cell.chefName.text = historyArr[indexPath.row].chef_full_name
        cell.customerName.text = historyArr[indexPath.row].customer_full_name
        let id = String(historyArr[indexPath.row].order_id!)
        cell.idNumber.text =  id
        cell.orderDate.text = historyArr[indexPath.row].updated_at
        if let product = historyArr[indexPath.row].product{
            cell.productName.text = product
        }
        cell.productPrice.text = String(historyArr[indexPath.row].total_price!)
        
        
        return cell
    }
    
}

extension HistoryViewController{
    
    func getDataOnServers()
    {
        loader()
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
        ]
        Alamofire.request(HISTORY, method :.get, headers: headers).responseData{
            response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                
                let rider_order = json["rider_order"]
                
                if rider_order.count != 0{
                    self.mainVu.isHidden = true
                    for index in rider_order.arrayValue{
                        self.historyArr.append(history(json: index))
                    }
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                        
                    }
                }else{
                    SVProgressHUD.dismiss()
                    self.mainLbl.text = "No History"
                    self.mainVu.isHidden = false
                    //dismiss(animated: true, completion: nil)
                    
                }

            
            case .failure(let error):
                print(error.localizedDescription)
            }
        }

        

    }
    
    
    
}

    

