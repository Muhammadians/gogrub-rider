//
//  HomeViewController.swift
//  GoGrub Rider
//
//  Created by apple on 10/16/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD
import Firebase
import FirebaseCore
import FirebaseDatabase
import CoreLocation
import SideMenuSwift
import SideMenu

class HomeViewController: UIViewController {

    //Mark :- Variables
    var isSideViewOpen: Bool = false
    var ref = DatabaseReference.init()
    var currentLocation : CLLocation!
    var locationManager:CLLocationManager!
    //Mark :- Arrays
    var getDataArr = [firebaseLatLng]()
    var notifyOrderArr = [notifyOrder]()
    //Mark :- outlets

    @IBOutlet weak var orderID: UILabel!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var customerNumberLbl: UILabel!
    @IBOutlet weak var customerIDLbl: UILabel!
    @IBOutlet weak var customerDeliveryTimeLbl: UILabel!
    @IBOutlet weak var customerAddressLbl: UILabel!
    
    
    @IBOutlet weak var chefNameLbl: UILabel!
    @IBOutlet weak var chefNumberLbl: UILabel!
    @IBOutlet weak var chefIDLbl: UILabel!
    @IBOutlet weak var chefMakingTimeLbl: UILabel!
    @IBOutlet weak var chefAddressLbl: UILabel!
    
    @IBOutlet weak var mainVu: UIView!
    @IBOutlet weak var mainLbl: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        
        self.navigationItem.title = "Home"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu (2)"), style: .plain, target: self, action: #selector(Menu))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cart"), style: .plain, target: self, action: #selector(icon))
    
        getDataOnServers()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getRiderCurrentLocation()
    }
    @objc func Menu(){
        sideMenuController?.revealMenu()
    }
    @objc func icon(){}
    @IBAction func tappedAcceptBtn(_ sender: Any) {
            self.performSegue(withIdentifier: "detail", sender: self)
    }
    
    @IBAction func tappedChefMapBtn(_ sender: Any) {
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(notifyOrderArr[0].chef_location_Lat!),\(notifyOrderArr[0].chef_location_Long!)&zoom=20&views=traffic&q=\(notifyOrderArr[0].chef_location_Lat!),\(String(describing: notifyOrderArr[0].chef_location_Long!))")!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(notifyOrderArr[0].chef_location_Lat!),\(String(describing: notifyOrderArr[0].chef_location_Long!))&zoom=20&views=traffic&q=\(notifyOrderArr[0].chef_location_Lat!),\(String(describing: notifyOrderArr[0].chef_location_Long!))")!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func tappedOrderMap(_ sender: Any) {
        
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
           UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(notifyOrderArr[0].customer_location_Lat!),\(notifyOrderArr[0].customer_location_Long!)&zoom=20&views=traffic&q=\(notifyOrderArr[0].customer_location_Lat!),\(String(describing: notifyOrderArr[0].customer_location_Long!))")!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(notifyOrderArr[0].customer_location_Lat!),\(String(describing: notifyOrderArr[0].customer_location_Long!))&zoom=20&views=traffic&q=\(notifyOrderArr[0].customer_location_Lat!),\(String(describing: notifyOrderArr[0].customer_location_Long!))")!, options: [:], completionHandler: nil)
        }

    }
    
   
}


extension HomeViewController{

    func getDataOnServers()
    {
        
        loader()
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
        ]
        Alamofire.request(NOTIFY_ORDER, method :.get, headers: headers).responseData{
            response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                
                let order = json["order"]
                if order.count != 0{
                    self.mainVu.isHidden = true
                    //for index in rider_order.arrayValue{
                    self.notifyOrderArr.append(notifyOrder(json: order))
                    //}
                    DispatchQueue.main.async {
                        self.puttingValues()
                        SVProgressHUD.dismiss()
                    }
                }else{
                    SVProgressHUD.dismiss()
                  self.mainVu.isHidden = false
                    self.mainLbl.text = json["message"].stringValue
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        
        
    }
    
    func puttingValues(){
        
        self.chefIDLbl.text = notifyOrderArr[0].chef_id
        self.customerIDLbl.text = notifyOrderArr[0].customer_id
        self.chefNameLbl.text = notifyOrderArr[0].chef_full_name
        self.customerNameLbl.text = notifyOrderArr[0].customer_full_name
        self.chefNumberLbl.text = notifyOrderArr[0].chef_phone
        self.customerNumberLbl.text = notifyOrderArr[0].customer_phone
        self.chefAddressLbl.text = notifyOrderArr[0].customer_address
        self.customerAddressLbl.text = notifyOrderArr[0].customer_address
        self.chefMakingTimeLbl.text = notifyOrderArr[0].estimate_delivery_mins
            
        self.customerDeliveryTimeLbl.text = notifyOrderArr[0].estimate_delivery_mins
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"{
            let des = segue.destination as! DetailViewController
           // des.detailOrderArr = notifyOrderArr
        }
        
    }
}












extension HomeViewController:CLLocationManagerDelegate{
  
    func getRiderCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.startUpdatingLocation()
        }else{
            print("goto setting permit")
        }
    }
    
    //Mark:- Get Current Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last as! CLLocation
        currentRiderLat = location.coordinate.latitude
        currentRiderLong = location.coordinate.longitude
        latLongInFirebase()
    }
    
    func latLongInFirebase(){
        loader()
      
        let dict = ["Lat":String(currentRiderLat),"Lng":String(currentRiderLong)]
        self.ref.child("Online_Drivers").child(rider_id).setValue(dict)
        SVProgressHUD.dismiss()
        
                
        
        
    }
    
    
}
