//
//  DocumentVerificationViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 04/11/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire



class DocumentVerificationViewController: UIViewController {

    
    //Mark :- Variables
    var image = UIImage()
    var imagePicker = UIImagePickerController()
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var photo: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
       
    }
    
    
    @IBAction func tappednextBtn(_ sender: Any) {
       upload()
        
    }
    
    @IBAction func tappedPhotoLibary(_ sender: Any) {
         openGallary()
    }
    
    
    @IBAction func tappedCameraBtn(_ sender: Any) {
        
        
        
        
    }
    
   

}


//Mark :- Image Picker Delegate Methods

extension DocumentVerificationViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func openGallary()
    {
        let alerts = UIAlertController.init(title: "Source", message: "Choose a Source File", preferredStyle: .actionSheet)
        //        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
        //            if UIImagePickerController.isSourceTypeAvailable(.camera){
        //                self.imagePicker.sourceType = .camera
        //            }
        //            self.present(self.imagePicker, animated: true, completion: nil)
        //        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .destructive, handler: { (action:UIAlertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
        
        present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        self.image = info[(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        let imageData = image.jpegData(compressionQuality: 0)
        let imgCompressed = UIImage(data: imageData!)
        
        image = imgCompressed!
        photo.image = image
        documentVerification[3] = image
        //performSegue(withIdentifier: "step4", sender: self)
        SVProgressHUD.dismiss()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}



extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}


extension DocumentVerificationViewController{
    
  
    
    func upload(){
        
        SVProgressHUD.show(withStatus: "Uploading Your Documents")
        
        let parameters = ["": ""]
        
    
        var mediaImgArr = [Media]()
        var imgTextArr = ["cnic_image[]","cnic_image[]","licence_image[]","licence_image[]"]
        for index in 0...documentVerification.count-1{
            mediaImgArr.append(Media(withImage: documentVerification[index], forKey: imgTextArr[index])!)
        }
        
        
        
        guard let url = URL(string: "http://207.148.113.150/index.php/api/v1/rider/apply") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = generateBoundary()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue("bearer \(UserDefaults.standard.object(forKey: "token") as! String)", forHTTPHeaderField: "Authorization")
        
        let dataBody = createDataBody(withParameters: parameters, media: mediaImgArr, boundary: boundary)
        request.httpBody = dataBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    DispatchQueue.main.async {
                        
                      
                        // self.getUserData()
                        
                               let alerts = UIAlertController.init(title: "Registration Successfully Please Login" , message: "", preferredStyle: .alert)
                             alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
                                self.present(vc, animated: true, completion: nil)
                           }))
                           self.present(alerts, animated: true, completion: nil)
                        
                        SVProgressHUD.dismiss()
                     //   self.navigationController?.popViewController(animated: true)
                        //self.collectionVu.reloadData()
                    }
                    
                } catch {
                    SVProgressHUD.dismiss()
                    self.alertPopup(title: error.localizedDescription)
                    print(error)
                }
            }
            }.resume()
    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters params: Parameters?, media: [Media]?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value)"+"\(lineBreak)")
            }
        }
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        return body
    }
}
