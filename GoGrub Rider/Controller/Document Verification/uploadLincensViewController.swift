//
//  uploadLincensViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 04/11/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class uploadLincensViewController: UIViewController {

    //Mark :- Variables
    var image = UIImage()
        var imagePicker = UIImagePickerController()
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var photo: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    imagePicker.delegate = self
        
    }
    
    @IBAction func tappednextBtn(_ sender: Any) {
        documentVerification[2] = image
        performSegue(withIdentifier: "step4", sender: self)
        
    }
    
    @IBAction func tappedPhotoLibary(_ sender: Any) {
         openGallary()
    }
    
    @IBAction func tappedCameraBtn(_ sender: Any) {



    }
    
    
    
}


//Mark :- Image Picker Delegate Methods

extension uploadLincensViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func openGallary()
    {
        let alerts = UIAlertController.init(title: "Source", message: "Choose a Source File", preferredStyle: .actionSheet)
        //        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
        //            if UIImagePickerController.isSourceTypeAvailable(.camera){
        //                self.imagePicker.sourceType = .camera
        //            }
        //            self.present(self.imagePicker, animated: true, completion: nil)
        //        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .destructive, handler: { (action:UIAlertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
        
        present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        self.image = info[(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        let imageData = image.jpegData(compressionQuality: 0)
        let imgCompressed = UIImage(data: imageData!)
        
        image = imgCompressed!
        photo.image = image
      
        SVProgressHUD.dismiss()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
