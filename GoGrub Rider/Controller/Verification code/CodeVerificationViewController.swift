//
//  CodeVerificationViewController.swift
//  GoGrub
//
//  Created by apple on 10/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class CodeVerificationViewController: UIViewController , UITextFieldDelegate {
    
    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var second: UITextField!
    @IBOutlet weak var third: UITextField!
    @IBOutlet weak var fourth: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  reSendCode()
      // codeVerification()
        
        first.addTarget(self, action: #selector(self.textDidChange(textfield:)), for: UIControl.Event.editingChanged)
        second.addTarget(self, action: #selector(self.textDidChange(textfield:)), for: UIControl.Event.editingChanged)
        third.addTarget(self, action: #selector(self.textDidChange(textfield:)), for: UIControl.Event.editingChanged)
        fourth.addTarget(self, action: #selector(self.textDidChange(textfield:)), for: UIControl.Event.editingChanged)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        
       
        //navigationItem.titleView = setcenterImg()
        //B780FAB3-0AA4-4887-B69E-16A6B2870630
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        first.becomeFirstResponder()
    }
    
    @objc func textDidChange(textfield : UITextField){
        let text = textfield.text
        
        if text?.utf16.count == 1{
            
            switch textfield{
                
            case first:
                second.becomeFirstResponder()
                break
            case second:
                third.becomeFirstResponder()
            case third:
                fourth.becomeFirstResponder()
            case fourth:
                fourth.becomeFirstResponder()
                
            default:
                break
                
            }
            
            
        }else{
            
        }
        
        
    }
    
    @objc func back(){self.navigationController?.popViewController(animated: true)}
    @objc func icon(){}
    
    @IBAction func tappedSendAgain(_ sender: Any) {
        
        print(first.text! + second.text! + third.text! + fourth.text!)
        
      // reSendCode()
        
    }
    
    
    @IBAction func tappedConformBtn(_ sender: Any) {
        codeVerification()
    }
    
}



extension CodeVerificationViewController{
    
   
    func codeVerification()
    {
      
            
            loader()
            let headers: HTTPHeaders = [
               "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
            ]
        let code = first.text! + second.text! + third.text! + fourth.text!
        Alamofire.request("http://207.148.113.150/index.php/api/v1/phone-verify", method: .post, parameters: ["confirmation_code":code], headers: headers).responseData{
                response in
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                     SVProgressHUD.dismiss()
                    print(json)
                    self.performSegue(withIdentifier: "docs", sender: self)
                    
                case .failure(let error):
                   
                    self.alertPopup(title: error.localizedDescription)
                    print(error.localizedDescription)
                }
            }
            
            
            
        
    }
    
    
    func reSendCode()
    {
        
        SVProgressHUD.show(withStatus: "Resending Code")
        
       // loader()
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
        ]
        Alamofire.request("http://207.148.113.150/index.php/api/v1/resend-verification-code", method :.post, headers: headers).responseData{
            response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                
               SVProgressHUD.dismiss()
                
                
            case .failure(let error):
                self.alertPopup(title: error.localizedDescription)
                print(error.localizedDescription)
            }
        }
        
        
        
    }
}

