//
//  SignupViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class SignupViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtPhoneNumber: CustomizationTextfield!
    
    @IBOutlet weak var txtConformPassword: CustomizationTextfield!
    @IBOutlet weak var txtPassword: CustomizationTextfield!
    @IBOutlet weak var txtEmail: CustomizationTextfield!
    @IBOutlet weak var txtName: CustomizationTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Register"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cart"), style: .plain, target: self, action: #selector(icon))
        
    
        if #available(iOS 12.0, *) {
            txtPassword.textContentType = UITextContentType.streetAddressLine2;
            txtConformPassword.textContentType = UITextContentType.streetAddressLine2;
        } else {
        }
            if #available(iOS 12.0, *) {
                txtPassword.textContentType = UITextContentType.oneTimeCode
                txtConformPassword.textContentType = UITextContentType.oneTimeCode
            } else {
        }
    }
    
    @objc func back(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func icon(){}
    
    @IBAction func tappedContinoue(_ sender: Any) {
        
        if txtName.text != "" && txtPhoneNumber.text != "" && txtEmail.text != "" && txtPassword.text != "" && txtConformPassword.text != "" {
            signUp()
        }else{
            alertPopup(title: "Please Fill All Fields")
        }
        
    }
    
    @IBAction func tappedSignInBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    


}


extension SignupViewController{
    
    func signUp()
    {
        loader()
        let url = URL(string: SIGNUP)
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        let parameterToSend = "full_name=" + txtName.text! + "&password_confirmation=" + txtConformPassword.text! + "&email=" + txtEmail.text! + "&password=" + txtPassword.text! + "&mobile=" + txtPhoneNumber.text!
        
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _:Data = data else{return}
            if error != nil{
                self.alertPopup(title: error!.localizedDescription)
                // alertPopup(title: error as! String)
            }
            
            let json : Any?
            
            do{
                json = try JSONSerialization.jsonObject(with: data!, options: [])
            }catch{
                return
            }
            
            guard let server_Response = json as? NSDictionary else{return}
            
            let data_block = server_Response["message"] as? String
            
            if data_block != nil{
               
               
                    DispatchQueue.main.async {
                        
                        let user = server_Response["user"] as! NSDictionary
                        rider_id = String(user["id"] as! Int)
                        rider_Name = user["full_name"] as! String
                        rider_Email = user["email"] as! String
                        rider_mobile
                            = user["mobile"] as! String
                        rider_rating = user["avg_rating"] as! String
                        applied_as_chef = user["applied_as_chef"] as! Bool
                        avg_reply_time = user["avg_reply_time"] as! Int
                        success_percentage = user["success_percentage"] as! String
                        UserDefaults.standard.set(server_Response["token"], forKey: "token")
                        let data = UserDefaults.standard.object(forKey: "token")
                        if let token = data as? String{
                            print(data)
                        }
                        
                        SVProgressHUD.dismiss()
                        self.performSegue(withIdentifier: "code", sender: self)
                    
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    self.alertPopup(title: "Check Your Email Or Phone Already Exist or cannot same password ")
                }
                    
                }
               
    
            
            }
        task.resume()
        
    }

}
