//
//  LoginViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class LoginViewController: UIViewController {

    //Mark :- Variables
    
    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtEmail: CustomizationTextfield!
    @IBOutlet weak var txtPassword: CustomizationTextfield!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        device_id = UIDevice.current.identifierForVendor!.uuidString
        print(device_id)
      
        
    }
    
    @IBAction func tappedContinueBtn(_ sender: Any) {
        
        if txtEmail.text != "" && txtPassword.text != ""{
            login()
        }else{
            alertPopup(title: "Please Fill All Fields")
        }
        
    }
    
   
    @IBAction func tappedForgetPassword(_ sender: Any) {
         performSegue(withIdentifier: "reset", sender: self)
    }
    
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        performSegue(withIdentifier: "signup", sender: self)
    }
}





extension LoginViewController{
    
    func login()
    {
        loader()
        let url = URL(string: LOGIN)
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        let parameterToSend = "mobile=" + txtEmail.text! + "&password=" + txtPassword.text! + "&device_id=" + device_id + "&device_type=" + "iPhone" + "&fcm_token=" + fcm_Token
        request.httpBody = parameterToSend.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _:Data = data else{return}
            if error != nil{self.alertPopup(title: error as! String)}
            
            let json : Any?
            do{json = try JSONSerialization.jsonObject(with: data!, options: [])}catch{return}
        
            guard let server_Response = json as? NSDictionary else{return}
            if let data_block = server_Response["message"] as? String{
                
                if  data_block == "Login Successfull."{
                    
                    DispatchQueue.main.async {
                        let user = server_Response["user"] as! NSDictionary
                        rider_id = String(user["id"] as! Int)
                        rider_Name = user["full_name"] as! String
                        rider_Email = user["email"] as! String
                        rider_mobile
                            = user["mobile"] as! String
                        rider_rating = user["avg_rating"] as! String
                        applied_as_chef = user["applied_as_chef"] as! Bool
                        avg_reply_time = user["avg_reply_time"] as! Int
                        success_percentage = user["success_percentage"] as! String
                        
                        
                        
                        //UserDefaults.standard.set(server_Response["token"], forKey: "token")
                        //let data = UserDefaults.standard.object(forKey: "token")
                      //  if let token = data as? String{
                        //    print(data)
                       // }
                        SVProgressHUD.dismiss()
                        self.performSegue(withIdentifier: "home", sender: self)
                    }
                }else{
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.alertPopup(title: "Login Failed")
                    }
                }
            }
        }
        task.resume()
    }
    
}
