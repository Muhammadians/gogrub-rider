//
//  ViewController.swift
//  GoGrub Rider
//
//  Created by apple on 10/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        animateSplash()
    }
    
    func animateSplash(){
        UIView.animate(withDuration: 1.5, animations: {
            self.logoImage.frame.size.width += 25
            self.logoImage.frame.size.height += 25
        }, completion: { (true) in
            self.fadeOut()
        })
    }
    
    func fadeOut()
    {
        UIView.animate(withDuration: 1.5, animations: {
            self.logoImage.frame.size.width -= 25
            self.logoImage.frame.size.height -= 25
        }) { (true) in
            self.performSegue(withIdentifier: "splashToHome", sender: self)
        }
    }
}

