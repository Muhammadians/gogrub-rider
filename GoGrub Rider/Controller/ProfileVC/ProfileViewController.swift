//
//  ProfileViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class ProfileViewController: UIViewController {

    
    //Mark :- Variables
    var imagePicker = UIImagePickerController()
    var check = false
    var dpData : UIImage!
    var selectedIndex = 0

    //Mark :- Arrays
    
    //Mark :- outlets
    @IBOutlet weak var txtName: CustomizationTextfield!
    @IBOutlet weak var txtEmail: CustomizationTextfield!
    @IBOutlet weak var txtMobile: CustomizationTextfield!
    @IBOutlet weak var imageVu: CustomizationImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Profile"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "go-back-left-arrow (1)"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "cart"), style: .plain, target: self, action: #selector(icon))
        
        imagePicker.delegate = self
        txtName.text = rider_Name
        txtEmail.text = rider_Email
        txtMobile.text = rider_mobile
        
    }
    
    @objc func back(){
      dismiss(animated: true, completion: nil)
    }
    
    @objc func icon(){
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedSaveBtn(_ sender: Any) {
        
        
    }
    
    @IBAction func tappedCameraBtn(_ sender: Any) {
        
        openGallary()
    }
}



//Mark :- Image Picker Delegate Methods

extension ProfileViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func openGallary()
    {
        let alerts = UIAlertController.init(title: "Source", message: "Choose a Source File", preferredStyle: .actionSheet)
        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .destructive, handler: { (action:UIAlertAction) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        alerts.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
        
        present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        
        let image = info[(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        let imageData = image.jpegData(compressionQuality: 0)
        let imgCompressed = UIImage(data: imageData!)
        
        dpData = imgCompressed
        
        self.imageVu.image = imgCompressed
        SVProgressHUD.show(withStatus: "Uploading")
       // self.upload(image: image)
       SVProgressHUD.dismiss()
        check = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}
