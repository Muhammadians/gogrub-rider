//
//  DetailViewController.swift
//  GoGrub Rider
//
//  Created by Muhammad Zunair on 24/10/2019.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD



class DetailViewController: UIViewController {

    //Mark :- Variables
    var lat = 0.0
    var long = 0.0
    //Mark :- Arrays
    var productData = [product]()
    //Mark :- outlets
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var deliveryCharges: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var makingTime: UILabel!
    @IBOutlet weak var deliveryTime: UILabel!
    @IBOutlet weak var customername: UILabel!
    @IBOutlet weak var customerPhone: UILabel!
    @IBOutlet weak var customerAdress: UILabel!
    @IBOutlet weak var tblVu: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        getDataOnServers()
       
    }
    
    @IBAction func tappedReceiveBtn(_ sender: Any) {
        
        
    }
    
    @IBAction func tappedMapBtn(_ sender: Any) {
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(lat),\(long)&zoom=20&views=traffic&q=\(lat),\(String(describing: long))")!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(lat),\(String(describing: long))&zoom=20&views=traffic&q=\(lat),\(String(describing: long))")!, options: [:], completionHandler: nil)
        }
        
    }
    
}

extension DetailViewController{
    
    func getDataOnServers()
    {
        
        loader()
        let headers: HTTPHeaders = [
            "Authorization" : "bearer \(UserDefaults.standard.object(forKey: "token") as! String)"
        ]
        Alamofire.request(CURRENT_ORDER, method :.get, headers: headers).responseData{
            response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                
                let order = json["order"]
                let order_Detail = order["detail"]
                let order_Detail1 = order_Detail[0]
                for index in order_Detail1.arrayValue{
                self.productData.append(product(json: index))
                }
                DispatchQueue.main.async {
                    
                    self.subTotal.text = order["subtotal"].stringValue + " Rs"
                    self.deliveryCharges.text = order["delivery_charges"].stringValue + " Rs"
                    self.total.text = order["total"].stringValue + " Rs"
                    self.makingTime.text = order[""].stringValue
                    self.deliveryTime.text = order["estimate_delivery_mins"].stringValue + " Minut"
                   
                    
                    self.customername.text = order["customer_full_name"].stringValue
                    self.customerPhone.text = order["customer_phone"].stringValue
                    self.customerAdress.text = order["customer_address"].stringValue
                    
                    
                let location = order["chef_location"]
                    let coordinates = location["coordinates"]
                    self.lat = coordinates[0].doubleValue
                    self.long = coordinates[1].doubleValue
                    
                    self.tblVu.reloadData()
                    SVProgressHUD.dismiss()
                    
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }

}



extension DetailViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "product") as! ProductTableViewCell
        
        //cell.img.image =
        if productData[indexPath.row].product != nil{
            cell.name.text = productData[indexPath.row].product
        }
        cell.price.text = productData[indexPath.row].price
        cell.quantity.text = "Quantity : " + productData[indexPath.row].qty!
                //cell..text = productData[indexPath.row].price
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    
}
